// Runtime
#include <persist/matrix/fs.hpp>
#include <persist/matrix/log.hpp>

#include <persist/fs/fs.hpp>

#include <persist/rsm/multipaxos/log/memory.hpp>
#include <persist/rsm/multipaxos/log/file.hpp>
#include <persist/rsm/multipaxos/log/segmented/log.hpp>

#include <persist/rsm/raft/log/memory.hpp>
#include <persist/rsm/raft/log/file.hpp>

#include <timber/log.hpp>

#include <wheels/io/read.hpp>
#include <wheels/memory/view_of.hpp>

#include <cassert>
#include <iostream>

using wheels::ViewOf;
using wheels::MutViewOf;

namespace rsm = persist::rsm;

//////////////////////////////////////////////////////////////////////

// Runtime

persist::matrix::LogBackend log_backend;
persist::matrix::FileSystem fs(&log_backend);

persist::fs::IFileSystem* Fs() {
  return &fs;
}

persist::matrix::FileSystem& FsImpl() {
  return fs;
}

timber::ILogBackend* LogBackend() {
  return &log_backend;
}



namespace random_access {

//////////////////////////////////////////////////////////////////////

void MemoryLogTest() {
  log_backend.Rotate("MemoryLog");

  rsm::multipaxos::MemoryLog memory_log;

  assert(memory_log.LastIndex() == 0);
  memory_log.Update(1, "1");
  memory_log.Update(3, "3");
  assert(memory_log.LastIndex() == 3);
  memory_log.Update(4, "4");

  {
    assert(memory_log.LastIndex() == 4);
    assert(memory_log.IsEmpty(2));
    assert(memory_log.TryRead(3) == "3");
    assert(memory_log.TryRead(4) == "4");
  }

  memory_log.TruncatePrefix(2);
  assert(memory_log.StartIndex() == 3);
  assert(memory_log.TryRead(3) == "3");
}

//////////////////////////////////////////////////////////////////////

void FileLogTest() {
  log_backend.Rotate("FileLog");

  auto log_path = Fs()->TmpPath() / "rsm" / "log" / "file";

  {
    rsm::multipaxos::FileLog file_log{Fs(), log_path};
    file_log.Open();
    file_log.Update(1, "1");
    file_log.Update(2, "2");
    file_log.Update(4, "4");
  }

  {
    rsm::multipaxos::FileLog file_log{Fs(), log_path};
    file_log.Open();
    assert(file_log.LastIndex() == 4);
    file_log.TruncatePrefix(2);
  }

  {
    rsm::multipaxos::FileLog file_log{Fs(), log_path};
    file_log.Open();

    assert(file_log.StartIndex() == 3);
    assert(file_log.LastIndex() == 4);
    assert(!file_log.TryRead(3).has_value());
    assert(file_log.TryRead(4) == "4");

    file_log.Update(3, "3");
    file_log.Update(5, "5");
    file_log.TruncatePrefix(3);
  }

  {
    rsm::multipaxos::FileLog file_log{Fs(), log_path};
    file_log.Open();

    assert(file_log.StartIndex() == 4);
    assert(file_log.LastIndex() == 5);
    assert(file_log.TryRead(4) == "4");
    assert(file_log.TryRead(5) == "5");
  }
}

//////////////////////////////////////////////////////////////////////

void SegmentedLogTest() {
  log_backend.Rotate("SegmentedLog");

  const auto log_dir = Fs()->TmpPath() / "rsm" / "segmented-log";

  rsm::multipaxos::SegmentedLog::Options log_options;
  log_options.segment_length = 2;

  {
    rsm::multipaxos::SegmentedLog log(Fs(), log_dir, log_options);
    log.Open();

    log.Update(1, "A");
    log.Update(2, "B");
    log.Update(4, "D");
    log.Update(3, "C");
    log.Update(2, "BB");
    log.Update(3, "CC");

    log.Update(7, "G");
  }

  // ["A", "BB", "CC", "D", E, E, "G"]

  {
    rsm::multipaxos::SegmentedLog log(Fs(), log_dir, log_options);
    log.Open();

    assert(log.TryRead(1) == "A");
    assert(log.TryRead(2) == "BB");
    assert(log.TryRead(3) == "CC");
    assert(log.TryRead(4) == "D");
    assert(log.TryRead(7) == "G");

    log.TruncatePrefix(3);

    log.Update(4, "DD");
    log.Update(7, "GG");
    log.Update(6, "F");
  }

  // [T, T, T, "DD", E, "F", "GG"]

  {
    rsm::multipaxos::SegmentedLog log(Fs(), log_dir, log_options);
    log.Open();

    assert(log.StartIndex() == 4);
    assert(log.TryRead(4) == "DD");
    assert(log.TryRead(6) == "F");
    assert(log.TryRead(7) == "GG");
  }
}

}  // namespace random_access

namespace contiguous {

//////////////////////////////////////////////////////////////////////

void MemoryLogTest() {
  log_backend.Rotate("RaftMemoryLog");

  rsm::raft::MemoryLog memory_log;

  assert(memory_log.Length() == 0);
  assert(memory_log.StartIndex() == 1);
  memory_log.Append({"1"});
  assert(memory_log.Length() == 1);
  memory_log.Append({"2", "3"});
  assert(memory_log.Length() == 3);
  memory_log.Append({"4", "5", "6"});
  assert(memory_log.Length() == 6);

  assert(memory_log.Read(1) == "1");
  assert(memory_log.Read(2) == "2");
  assert(memory_log.Read(3) == "3");
  assert(memory_log.Read(4) == "4");
  assert(memory_log.Read(5) == "5");
  assert(memory_log.Read(6) == "6");

  memory_log.TruncateSuffix(/*from_index=*/3);
  assert(memory_log.Length() == 2);
  assert(memory_log.Read(2) == "2");
  assert(memory_log.Read(1) == "1");

  memory_log.TruncateSuffix(/*from_index=*/1);
  assert(memory_log.Length() == 0);
  memory_log.Append({"a", "b", "c"});
  memory_log.Append({"d", "e"});
  assert(memory_log.Length() == 5);

  memory_log.TruncatePrefix(/*end_index=*/1);
  assert(memory_log.StartIndex() == 2);
  assert(memory_log.Length() == 5);
  assert(memory_log.Read(2) == "b");
  assert(memory_log.Read(3) == "c");
  assert(memory_log.Read(4) == "d");

  memory_log.TruncatePrefix(2);
  assert(memory_log.StartIndex() == 3);
  assert(memory_log.Read(3) == "c");

  memory_log.TruncateSuffix(/*from_index=*/3);
  assert(memory_log.StartIndex() == 3);
  assert(memory_log.Length() == 2);
}

//////////////////////////////////////////////////////////////////////

void FileLogTest() {
  log_backend.Rotate("RaftFileLog");

  const auto log_path = Fs()->TmpPath() / "rsm" / "raft" / "file-log";

  {
    rsm::raft::FileLog log(Fs(), log_path);
    log.Open();

    assert(log.Length() == 0);
    assert(log.StartIndex() == 1);
    log.Append({"1"});
    assert(log.Length() == 1);
    log.Append({"2", "3"});
    assert(log.Length() == 3);
    log.Append({"4", "5", "6"});
    assert(log.Length() == 6);

    assert(log.Read(1) == "1");
    assert(log.Read(2) == "2");
    assert(log.Read(3) == "3");
    assert(log.Read(4) == "4");
    assert(log.Read(5) == "5");
    assert(log.Read(6) == "6");

    log.TruncateSuffix(/*from_index=*/3);
  }

  {
    rsm::raft::FileLog log(Fs(), log_path);
    log.Open();

    assert(log.Length() == 2);
    assert(log.Read(2) == "2");
    assert(log.Read(1) == "1");

    log.TruncateSuffix(/*from_index=*/1);
  }

  {
    rsm::raft::FileLog log(Fs(), log_path);
    log.Open();

    assert(log.Length() == 0);
    log.Append({"a", "b", "c"});
    log.Append({"d", "e"});
    assert(log.Length() == 5);
  }

  {
    rsm::raft::FileLog log(Fs(), log_path);
    log.Open();

    log.TruncatePrefix(/*end_index=*/1);
    assert(log.StartIndex() == 2);
    assert(log.Length() == 5);
    assert(log.Read(2) == "b");
    assert(log.Read(3) == "c");
    assert(log.Read(4) == "d");

    log.TruncatePrefix(2);
    assert(log.StartIndex() == 3);
    assert(log.Read(3) == "c");

    log.TruncateSuffix(/*from_index=*/3);
    assert(log.StartIndex() == 3);
    assert(log.Length() == 2);
  }
}

//////////////////////////////////////////////////////////////////////

}  // namespace contiguous

int main() {
  random_access::MemoryLogTest();
  random_access::FileLogTest();
  random_access::SegmentedLogTest();

  contiguous::MemoryLogTest();
  contiguous::FileLogTest();

  return 0;
}
