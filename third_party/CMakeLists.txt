include(FetchContent)

# --------------------------------------------------------------------

# set(FETCHCONTENT_FULLY_DISCONNECTED ON)
# set(FETCHCONTENT_QUIET OFF)

# --------------------------------------------------------------------

FetchContent_Declare(
        wheels
        GIT_REPOSITORY https://gitlab.com/Lipovsky/wheels.git
        GIT_TAG master
)
FetchContent_MakeAvailable(wheels)

# --------------------------------------------------------------------

FetchContent_Declare(
        fallible
        GIT_REPOSITORY https://gitlab.com/Lipovsky/fallible.git
        GIT_TAG fallible
)
FetchContent_MakeAvailable(fallible)

# --------------------------------------------------------------------

FetchContent_Declare(
        rewrite
        GIT_REPOSITORY https://gitlab.com/whirl-framework/rewrite.git
        GIT_TAG master
)
FetchContent_MakeAvailable(rewrite)

# --------------------------------------------------------------------

FetchContent_Declare(
        timber
        GIT_REPOSITORY https://gitlab.com/whirl-framework/timber.git
        GIT_TAG master
)
FetchContent_MakeAvailable(timber)

# --------------------------------------------------------------------

set (CRC32C_BUILD_TESTS OFF CACHE INTERNAL "Turn off tests")
set (CRC32C_BUILD_BENCHMARKS OFF CACHE INTERNAL "Turn off benchmarks")
set (CRC32C_USE_GLOG OFF CACHE INTERNAL "Do not use glog")
set (CRC32C_INSTALL OFF CACHE INTERNAL "Do not install")

FetchContent_Declare(
        crc32c
        GIT_REPOSITORY https://github.com/google/crc32c.git
        GIT_TAG main
)
FetchContent_MakeAvailable(crc32c)
