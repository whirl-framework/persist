#pragma once

#include <wheels/memory/view.hpp>

namespace persist::log {

extern const uint32_t kHeaderMagicNumber;

struct Header {
  uint32_t magic;
  uint32_t size;
  uint64_t checksum;
};

inline wheels::MutableMemView MutViewOf(Header& header) {
  return {(char*)&header, sizeof(Header)};
}

}  // namespace persist::log
