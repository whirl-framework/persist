#include <persist/log/writer.hpp>

#include <persist/log/checksum.hpp>

#include <fallible/result/make.hpp>

using fallible::Ok;
using fallible::PropagateError;

namespace persist::log {

LogWriter::LogWriter(const fs::Path& file_path)
    : fs_(file_path.Fs()), file_path_(file_path), writer_(file_path) {
}

Header LogWriter::MakeHeader(wheels::ConstMemView entry) {
  return {kHeaderMagicNumber, (uint32_t)entry.Size(), ComputeChecksum(entry)};
}

void LogWriter::WriteToBuffer(wheels::ConstMemView entry) {
  // Header
  auto header = MakeHeader(entry);
  buffer_.Append(MutViewOf(header));
  // Entry
  buffer_.Append(entry);
}

fallible::Status LogWriter::Open(size_t offset) {
  if (fs_.Exists(file_path_)) {
    // Truncate
    auto status = fs_.Truncate(file_path_, offset);
    if (!status.IsOk()) {
      return PropagateError(status);
    }
  } else {
    // Create new
    {
      auto status = fs_.Create(file_path_);
      if (!status.IsOk()) {
        return PropagateError(status);
      }
    }
    // Persist directory changes
    {
      auto status = fs_.SyncDir(file_path_.Parent());
      if (!status.IsOk()) {
        return PropagateError(status);
      }
    }
  }

  // Open for append
  {
    auto status = writer_.Open();
    if (!status.IsOk()) {
      return PropagateError(status);
    }
  }

  offset_ = offset;

  return Ok();
}

fallible::Status LogWriter::Append(wheels::ConstMemView entry,
                                 WriteOptions options) {
  buffer_.Clear();
  WriteToBuffer(entry);

  {
    auto status = writer_.Write(buffer_.View());
    if (!status.IsOk()) {
      return PropagateError(status);
    }
  }

  offset_ += buffer_.Size();

  if (options.sync) {
    return writer_.Sync();
  } else {
    return Ok();
  }
}

}  // namespace persist::log
