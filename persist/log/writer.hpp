#pragma once

#include <persist/log/write_options.hpp>

#include <persist/fs/fs.hpp>
#include <persist/fs/io/file_writer.hpp>

#include <persist/log/header.hpp>

#include <wheels/memory/buffer.hpp>

namespace persist::log {

// Write-ahead log writer

class LogWriter {
 public:
  LogWriter(const fs::Path& file_path);

  // One-shot
  // Truncate log file to `offset` and open for writing
  fallible::Status Open(size_t offset);

  fallible::Status Append(wheels::ConstMemView entry,
                          WriteOptions options = WriteOptions());

  const fs::Path& Path() const {
    return file_path_;
  }

  size_t CurrentOffset() const {
    return offset_;
  }

 private:
  static Header MakeHeader(wheels::ConstMemView entry);
  void WriteToBuffer(wheels::ConstMemView entry);

 private:
  fs::IFileSystem& fs_;
  fs::Path file_path_;

  wheels::GrowingBuffer buffer_;
  fs::FileWriter writer_;

  size_t offset_ = 0;
};

}  // namespace persist::log
