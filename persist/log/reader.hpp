#pragma once

#include <persist/fs/fs.hpp>
#include <persist/fs/io/file_reader.hpp>

#include <persist/log/header.hpp>

#include <rewrite/read/buffered.hpp>

#include <wheels/memory/view.hpp>

#include <optional>
#include <string>

namespace persist::log {

// Write-ahead log reader

class LogReader {
 public:
  LogReader(const fs::Path& file_path);

  std::optional<std::string> ReadNext();

  // Start offset for LogWriter
  size_t OffsetForWriter() const {
    return offset_;
  }

 private:
  rewrite::IReader& LogFileReader() {
    return buf_file_reader_;
  }

  void Open();

  std::optional<Header> ReadHeader();
  std::optional<std::string> ReadEntry(const Header& header);

  bool ReadNextPart(wheels::MutableMemView part);

 private:
  fs::FileReader file_reader_;
  rewrite::BufferedReader buf_file_reader_;
  size_t offset_{0};
  bool done_{false};
};

}  // namespace persist::log
