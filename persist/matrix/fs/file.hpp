#pragma once

#include <wheels/memory/view.hpp>

#include <vector>

namespace persist::fs::matrix {

class File {
 public:
  size_t Length() const;
  void Truncate(size_t new_length);
  void Append(wheels::ConstMemView data);
  size_t PRead(size_t offset, wheels::MutableMemView buffer) const;

  size_t ComputeDigest() const;

 private:
  wheels::ConstMemView Tail(size_t offset) const;

 private:
  std::vector<char> data_;
};

}  // namespace persist::fs::matrix
