#include <persist/fs/io/file_writer.hpp>

#include <fallible/result/make.hpp>

using fallible::Ok;
using fallible::PropagateError;

namespace persist::fs {

//////////////////////////////////////////////////////////////////////

FileWriter::FileWriter(Path path)
    : fs_(path.Fs()), path_(std::move(path)) {
}

FileWriter::FileWriter(FileWriter&& writer)
    : fs_(writer.fs_), path_(writer.path_),
      fd_(writer.ReleaseFd()) {
}

FileWriter::~FileWriter() {
  if (IsValid()) {
    Close().ExpectOk("Failed to close file");
  }
}

fallible::Status FileWriter::Open() {
  auto fd = fs_.Open(path_, FileMode::Append);
  if (fd.IsOk()) {
    fd_ = *fd;
    return Ok();
  } else {
    fd_ = -1;
    return PropagateError(fd);
  }
}

fallible::Status FileWriter::Sync() {
  EnsureValid();
  return fs_.Sync(fd_);
}

fallible::Status FileWriter::Write(wheels::ConstMemView data) {
  EnsureValid();
  return fs_.Append(fd_, data);
}

fallible::Status FileWriter::Flush() {
  // Nothing to do
  return Ok();
}

fallible::Status FileWriter::Close() {
  EnsureValid();
  return fs_.Close(std::exchange(fd_, -1));
}

bool FileWriter::IsValid() const {
  return fd_ != -1;
}

void FileWriter::EnsureValid() {
  WHEELS_VERIFY(IsValid(), "Invalid state");
}

Fd FileWriter::ReleaseFd() {
  return std::exchange(fd_, -1);
}

//////////////////////////////////////////////////////////////////////

fallible::Result<FileWriter> Write(Path path) {
  FileWriter writer(std::move(path));
  auto status = writer.Open();

  if (status.IsOk()) {
    return Ok(std::move(writer));
  } else {
    return PropagateError(status);
  }
}

}  // namespace persist::fs
