#pragma once

#include <string>
#include <vector>
#include <ostream>
#include <functional>

namespace persist::fs {

//////////////////////////////////////////////////////////////////////

struct IFileSystem;

class Path {
 public:
  Path(IFileSystem& fs, std::string repr)
      : fs_(&fs), repr_(std::move(repr)) {
  }

  // Workaround
  //  Path(std::string repr)
  //      : fs_(nullptr), repr_(std::move(repr)) {
  //  }

  // Returns implementation-specific representation
  const std::string& Repr() const {
    return repr_;
  }

  bool IsRootPath() const;

  // Precondition: IsRootPath() == false
  std::string Name() const;

  // Precondition: IsRootPath() == false
  Path Parent() const;

  bool operator==(const Path& that) const {
    return repr_ == that.repr_;
  }

  bool operator!=(const Path& that) const {
    return !(*this == that);
  }

  // For ordered containers
  bool operator<(const Path& path) const {
    return repr_ < path.repr_;
  }

  std::size_t ComputeHash() const noexcept {
    return std::hash<std::string>()(repr_);
  }

  Path& operator/=(const std::string& name);

  IFileSystem& Fs() const;

 private:
  IFileSystem* fs_;
  std::string repr_;
};

// Usage: dir / "foo" / "bar"
inline Path operator/(Path path, const std::string& name) {
  path /= name;
  return path;
}

inline std::ostream& operator<<(std::ostream& out, const Path& path) {
  out << '"' << path.Repr() << '"';
  return out;
}

using FileList = std::vector<Path>;

}  // namespace persist::fs

//////////////////////////////////////////////////////////////////////

namespace std {

template <>
struct hash<persist::fs::Path> {
  std::size_t operator()(const persist::fs::Path& path) const noexcept {
    return path.ComputeHash();
  }
};

}  // namespace std
