// Runtime
#include <persist/matrix/fs/fs.hpp>

#include <persist/fs/fs.hpp>

#include <persist/fs/io/file_reader.hpp>
#include <persist/fs/io/file_writer.hpp>

#include <timber/log/log.hpp>
#include <timber/log/backends/stdout/backend.hpp>

#include <rewrite/read/read.hpp>

#include <wheels/memory/view_of.hpp>

#include <cassert>

//////////////////////////////////////////////////////////////////////

// Runtime

timber::backends::StdoutLogBackend log_backend;
persist::fs::matrix::FileSystem fs(log_backend);

persist::fs::IFileSystem& Fs() {
  return fs;
}

timber::ILogBackend& LogBackend() {
  return log_backend;
}

//////////////////////////////////////////////////////////////////////

int main() {
  timber::Logger logger_("main", LogBackend());

  auto tmp = Fs().TempPath();

  auto fpath = tmp / "file";

  assert(fpath.Name() == "file");
  assert(fpath.Parent() == tmp);

  {
    assert(!Fs().Exists(fpath));

    bool new_file = Fs().Create(fpath).ExpectValueOr("Failed to create file");
    assert(new_file);

    assert(Fs().Exists(fpath));

    auto writer = persist::fs::Write(fpath).ExpectValueOr("Failed to open file");

    writer.Write(wheels::ViewOf("Hello")).ExpectOk("Failed to write to file");
    writer.Write(wheels::ViewOf(", ")).ExpectOk("Failed to write to file");
    writer.Write(wheels::ViewOf("world!")).ExpectOk("Failed to write to file");
  }

  {
      auto files = Fs().ListFiles("/");
      for (const auto& path : files) {
        TIMBER_LOG_INFO("File: {}", path);
      }
  }

  {
    auto reader = persist::fs::Read(fpath).ExpectValueOr("Failed to open file");
    auto content = rewrite::ReadAll(reader).ExpectValueOr("Failed to read from file");

    TIMBER_LOG_INFO("Content of {}: '{}'", fpath, content);
  }

  Fs().Unlink(fpath).ExpectOk();
  assert(!Fs().Exists(fpath));

  return 0;
}
